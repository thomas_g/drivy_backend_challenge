# frozen_string_literal: true

require './lib/rental_summary'

RSpec.describe RentalsSummaryGeneratorService do
  describe '#call' do
    subject(:generator_service) { described_class.new(input_path, 'output.json').call }

    let(:input_path) { 'spec/fixtures/input.json' }

    it 'creates a rental summary json file' do
      expected_content = File.read('spec/fixtures/expected_output.json')

      output_file = instance_double(File, 'output file')
      allow(File).to receive(:open).with('output.json', 'w').and_yield(output_file)
      expect(output_file).to receive(:write).with(expected_content)
      generator_service
    end

    describe 'with a path that does not exist' do
      let(:input_path) { 'unknown.json' }

      it { expect { generator_service }.to raise_exception(Errno::ENOENT) }
    end

    describe 'with file malformated' do
      let(:input_path) { 'spec/fixtures/invalid.json' }

      it { expect { generator_service }.to raise_exception(JSON::ParserError) }
    end
  end
end
