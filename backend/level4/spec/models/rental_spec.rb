# frozen_string_literal: true

require './lib/rental_summary'

RSpec.describe Rental do
  subject(:rental) { described_class.new(rental_attributes) }

  let(:car) { Car.new(id: 4, price_per_day: 1000, price_per_km: 10) }
  let(:rental_attributes) do
    {
      id: 6,
      car: car,
      start_date: Date.new(2019, 04, 1),
      end_date: Date.new(2019, 04, 8),
      distance: 100
    }
  end

  describe 'Attributes' do
    it { expect(rental.id).to eq(6) }
    it { expect(rental.car).to eq(car) }
    it { expect(rental.start_date).to eq(Date.new(2019, 04, 1)) }
    it { expect(rental.end_date).to eq(Date.new(2019, 04, 8)) }
    it { expect(rental.distance).to eq(100) }
  end

  describe 'Validations' do
    it 'raises an ArgumentError when :id is nil' do
      rental_attributes[:id] = nil
      expect { rental }.to raise_exception(ArgumentError, 'Id can\'t be blank.')
    end
    it 'raises an ArgumentError when :id is a float' do
      rental_attributes[:id] = 6.14
      expect { rental }.to raise_exception(ArgumentError, 'Id must be of type Integer.')
    end
    it 'raises an ArgumentError when :id is equal 0' do
      rental_attributes[:id] = 0
      expect { rental }.to raise_exception(ArgumentError, 'Id must be > 0.')
    end
    it 'raises an ArgumentError when :car is nil' do
      rental_attributes[:car] = nil
      expect { rental }.to raise_exception(ArgumentError, 'Car can\'t be blank.')
    end
    it 'raises an ArgumentError when :car is a string' do
      rental_attributes[:car] = 'car'
      expect { rental }.to raise_exception(ArgumentError, 'Car must be of type Car.')
    end
    it 'raises an ArgumentError when :start_date is nil' do
      rental_attributes[:start_date] = nil
      expect { rental }.to raise_exception(ArgumentError, 'Start_date can\'t be blank.')
    end
    it 'raises an ArgumentError when :start_date is a string' do
      rental_attributes[:start_date] = '1998-12-24'
      expect { rental }.to raise_exception(ArgumentError, 'Start_date must be of type Date.')
    end
    it 'raises an ArgumentError when :end_date is nil' do
      rental_attributes[:end_date] = nil
      expect { rental }.to raise_exception(ArgumentError, 'End_date can\'t be blank.')
    end
    it 'raises an ArgumentError when :end_date is a string' do
      rental_attributes[:end_date] = '1998-12-24'
      expect { rental }.to raise_exception(ArgumentError, 'End_date must be of type Date.')
    end
    it 'raises an ArgumentError when :start_date smaller than :end_date is not a date' do
      rental_attributes[:start_date] = Date.new(2019, 04, 1)
      rental_attributes[:end_date] = Date.new(2019, 03, 1)
      expect { rental }.to raise_exception(ArgumentError, 'End_date must be >= start_date.')
    end
    it 'is valid when :start_date smaller equal :end_date' do
      rental_attributes[:start_date] = Date.new(2019, 04, 1)
      rental_attributes[:end_date] = Date.new(2019, 04, 1)
      expect(rental).to be_a described_class
    end
    it 'raises an ArgumentError when :distance is nil' do
      rental_attributes[:distance] = nil
      expect { rental }.to raise_exception(ArgumentError, 'Distance can\'t be blank.')
    end
    it 'raises an ArgumentError when :distance is a float' do
      rental_attributes[:distance] = 12.3
      expect { rental }.to raise_exception(ArgumentError, 'Distance must be of type Integer.')
    end
    it 'raises an ArgumentError when :distance is smaller than 0' do
      rental_attributes[:distance] = -8
      expect { rental }.to raise_exception(ArgumentError, 'Distance must be >= 0.')
    end
    it 'is valid when :distance equal 0' do
      rental_attributes[:distance] = 0
      expect(rental).to be_a described_class
    end
  end

  describe 'Methods' do
    describe '#price' do
      it 'calculates the price for a rental period of 1 day' do
        rental_attributes[:end_date] = Date.new(2019, 04, 1)
        expect(rental.price).to eq(2000)
      end
      it 'calculates the price for a rental period of 2 day' do
        rental_attributes[:end_date] = Date.new(2019, 04, 2)
        expect(rental.price).to eq(2900)
      end
      it 'calculates the price for a rental period of 5 day' do
        rental_attributes[:end_date] = Date.new(2019, 04, 5)
        expect(rental.price).to eq(5400)
      end
      it 'calculates the price for a rental period of 20 day' do
        rental_attributes[:end_date] = Date.new(2019, 04, 20)
        expect(rental.price).to eq(13_900)
      end
    end

    describe '#fees_and_commissions' do
      subject(:commissions) { rental.fees_and_commissions }

      let(:rental) { described_class.new(rental_attributes) }

      it 'calculates commissions for a price of 12000 and 8 days' do
        allow(rental).to receive(:price).and_return(12_000)
        expected_result = { driver: -12_000, owner: 8400, insurance: 1800,
                            assistance: 800, drivy: 1000 }
        expect(commissions).to eq(expected_result)
      end

      it 'calculates commissions for a price of 12000 and 1 days' do
        rental_attributes[:end_date] = Date.new(2019, 04, 1)
        allow(rental).to receive(:price).and_return(12_000)
        expected_result = { driver: -12_000, owner: 8400, insurance: 1800,
                            assistance: 100, drivy: 1700 }
        expect(commissions).to eq(expected_result)
      end

      it 'calculates commissions for a price of 1000 and 8 days' do
        allow(rental).to receive(:price).and_return(1000)
        expected_result = { driver: -1000, owner: 700, insurance: 150,
                            assistance: 800, drivy: -650 }
        expect(commissions).to eq(expected_result)
      end
    end

    describe '#actions' do
      subject(:actions) { rental.actions }

      let(:rental) { described_class.new(rental_attributes) }

      context 'when the value is negative' do
        before do
          allow(rental).to receive(:fees_and_commissions).and_return(foo: -42)
        end

        it { expect(actions.first[:who]).to eq('foo') }
        it { expect(actions.first[:type]).to eq('debit') }
        it { expect(actions.first[:amount]).to eq(42) }
      end

      context 'when the value is positive' do
        before do
          allow(rental).to receive(:fees_and_commissions).and_return(bar: 42)
        end

        it { expect(actions.first[:who]).to eq('bar') }
        it { expect(actions.first[:type]).to eq('credit') }
        it { expect(actions.first[:amount]).to eq(42) }
      end
    end

    describe 'to_hash' do
      subject(:rental_to_hash) { rental.to_hash }

      let(:rental) { described_class.new(rental_attributes) }

      before { allow(rental).to receive(:actions).and_return('foo') }

      it { expect(rental_to_hash).to eq(id: rental_attributes[:id], actions: 'foo') }
    end
  end
end
