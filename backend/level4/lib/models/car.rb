# frozen_string_literal: true

class Car
  attr_reader :id, :price_per_day, :price_per_km

  ATTRIBUTES = {
    id: {
      presence: true,
      type: Integer,
      numericality: '> 0'
    },
    price_per_day: {
      presence: true,
      type: Integer,
      numericality: '> 0'
    },
    price_per_km: {
      presence: true,
      type: Integer,
      numericality: '> 0'
    }
  }.freeze

  private

  def initialize(params = {})
    @id = params[:id]
    @price_per_day = params[:price_per_day]
    @price_per_km = params[:price_per_km]
    validate
  end

  def validate
    ATTRIBUTES.each do |attr_name, validation|
      attribute = send(attr_name)
      raise ArgumentError, "#{attr_name.capitalize} can't be blank." if
        validation[:presence] && attribute.nil?
      raise ArgumentError, "#{attr_name.capitalize} must be of type #{validation[:type]}." unless
        validation[:type].nil? || attribute.is_a?(validation[:type])
      raise ArgumentError, "#{attr_name.capitalize} must be #{validation[:numericality]}." if
        validation[:numericality] && !eval("#{attr_name} #{validation[:numericality]}")
    end
  end
end
