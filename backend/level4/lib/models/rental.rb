# frozen_string_literal: true

class Rental
  attr_reader :id, :car, :start_date, :end_date, :distance

  ATTRIBUTES = {
    id: {
      presence: true,
      type: Integer,
      numericality: '> 0'
    },
    car: {
      presence: true,
      type: Car
    },
    start_date: {
      presence: true,
      type: Date
    },
    end_date: {
      presence: true,
      type: Date,
      numericality: '>= start_date'
    },
    distance: {
      presence: true,
      type: Integer,
      numericality: '>= 0'
    }
  }.freeze

  def price
    distance_rate + duration_rate
  end

  def fees_and_commissions
    {
      driver: price * -1,
      owner: (price * 0.70).to_i,
      insurance: (price * 0.15).to_i,
      assistance: duration * 100,
      drivy: (price * 0.15 - duration * 100).to_i
    }
  end

  def actions
    fees_and_commissions.map do |actor, value|
      {
        who: actor.to_s,
        type: value.negative? ? 'debit' : 'credit',
        amount: value.abs
      }
    end
  end

  def to_hash
    {
      id: id,
      actions: actions
    }
  end

  private

  def initialize(params = {})
    @id = params[:id]
    @car = params[:car]
    @start_date = params[:start_date]
    @end_date = params[:end_date]
    @distance = params[:distance]
    validate
  end

  def validate
    ATTRIBUTES.each do |attr_name, validation|
      attribute = send(attr_name)
      raise ArgumentError, "#{attr_name.capitalize} can't be blank." if
        validation[:presence] && attribute.nil?
      raise ArgumentError, "#{attr_name.capitalize} must be of type #{validation[:type]}." unless
        validation[:type].nil? || attribute.is_a?(validation[:type])
      raise ArgumentError, "#{attr_name.capitalize} must be #{validation[:numericality]}." if
        validation[:numericality] && !eval("#{attr_name} #{validation[:numericality]}")
    end
  end

  def duration
    (end_date - start_date).to_i + 1
  end

  def distance_rate
    distance * car.price_per_km
  end

  def duration_rate
    duration = (end_date - start_date).to_i + 1
    # apply discount after a certain amount of days
    duration_rate = 0
    duration.times do |day|
      duration_rate += case day
                       when 0
                         car.price_per_day
                       when 1..3
                         car.price_per_day * 0.9
                       when 4..9
                         car.price_per_day * 0.7
                       else
                         car.price_per_day * 0.5
                       end
    end
    duration_rate.to_i
  end
end
