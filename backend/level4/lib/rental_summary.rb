# frozen_string_literal: true

require 'json'
require 'date'
require_relative 'models/car'
require_relative 'models/rental'
require_relative 'services/rentals_summary_generator_service'
require_relative 'services/rentals_parser_service'

module RentalSummary; end
