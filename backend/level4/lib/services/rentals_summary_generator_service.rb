# frozen_string_literal: true

class RentalsSummaryGeneratorService
  def call
    rentals = get_rentals_from_hash(input_hash)
    write_to_json_file output_hash(rentals)
  end

  private

  def initialize(input_path, output_path)
    @input_path = input_path
    @output_path = output_path
  end

  def input_hash
    input_file = File.read(@input_path)
    JSON.parse(input_file, symbolize_names: true)
  end

  def get_rentals_from_hash(input_hash)
    RentalsParserService.new(input_hash).call
  end

  def output_hash(rentals)
    { rentals: rentals.map(&:to_hash) }
  end

  def write_to_json_file(hash)
    File.open(@output_path, 'w') do |f|
      # Always end a text file with a eol char (\n)
      f.write(JSON.pretty_generate(hash) + "\n")
    end
  end
end
