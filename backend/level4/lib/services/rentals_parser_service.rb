# frozen_string_literal: true

class RentalsParserService
  def call
    parse_cars
    parse_rentals
  end

  private

  def initialize(input_hash)
    @input_hash = input_hash

    validate_cars(input_hash[:cars])
    validate_rentals(input_hash[:rentals])
  end

  def validate_cars(cars_arr)
    raise ArgumentError, 'Cars must be presents.' if cars_arr.nil? || cars_arr.empty?
    raise ArgumentError, 'Car ids must be unique.' if
      cars_arr.map { |car| car[:id] }.uniq.count != cars_arr.length
  end

  def validate_rentals(rentals_arr)
    raise ArgumentError, 'Rentals must be presents.' if rentals_arr.nil? || rentals_arr.empty?
    raise ArgumentError, 'Rental ids must be unique.' if
      rentals_arr.map { |rental| rental[:id] }.uniq.count != rentals_arr.length
  end

  def parse_cars
    @cars = @input_hash[:cars].map { |car| Car.new(car) }
  end

  def parse_rentals
    rentals = []
    @input_hash[:rentals].each do |rental|
      car = @cars.find { |c| c.id == rental[:car_id] }
      raise 'no car' unless car

      new_rental = Rental.new(
        id: rental[:id],
        car: car,
        start_date: Date.strptime(rental[:start_date], '%Y-%m-%d'),
        end_date: Date.strptime(rental[:end_date], '%Y-%m-%d'),
        distance: rental[:distance]
      )
      check_overlapping_period(rentals, new_rental)
      rentals << new_rental
    end
    rentals
  end

  def check_overlapping_period(rentals, new_rental)
    # Check if there is already a rental with the same car that has an
    # overlapping rental period
    car = new_rental.car
    rentals.find_all { |r| r.car == car }.each do |candidate|
      msg = "Rental periods can't be overlapping for the same car (car_id: #{car.id})."
      raise ArgumentError, msg if
        new_rental.start_date <= candidate.end_date &&
        candidate.start_date <= new_rental.end_date
    end
  end
end
