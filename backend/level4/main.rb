# frozen_string_literal: true

require_relative 'lib/rental_summary'

RentalsSummaryGeneratorService.new('data/input.json', 'data/output.json').call
