# frozen_string_literal: true

require './lib/rental_summary'

RSpec.describe RentalsPresenter do
  subject(:presenter) { described_class.new(rentals) }

  let(:car) { Car.new(id: 4, price_per_day: 1000, price_per_km: 10) }
  let(:rentals) do
    [
      Rental.new(id: 6,
                 car: car,
                 start_date: Date.new(2019, 04, 1),
                 end_date: Date.new(2019, 04, 8),
                 distance: 100,
                 options: []),
      Rental.new(id: 7,
                 car: car,
                 start_date: Date.new(2019, 04, 1),
                 end_date: Date.new(2019, 04, 8),
                 distance: 100,
                 options: [])
    ]
  end

  it { expect(presenter.as_hash).to be_a(Hash) }
  it { expect(presenter.as_hash.keys).to eq([:rentals]) }
  it { expect(presenter.as_hash.values.first.length).to eq(2) }
end
