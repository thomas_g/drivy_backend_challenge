# frozen_string_literal: true

require './lib/rental_summary'

RSpec.describe ActionPresenter do
  subject(:presenter) { described_class.new(actor, value) }

  let(:actor) { 'drivy' }

  context 'when the value is negative' do
    let(:value) { -42 }

    it { expect(presenter.as_hash[:who]).to eq('drivy') }
    it { expect(presenter.as_hash[:type]).to eq('debit') }
    it { expect(presenter.as_hash[:amount]).to eq(42) }
  end

  context 'when the value is positive' do
    let(:value) { 42 }

    it { expect(presenter.as_hash[:who]).to eq('drivy') }
    it { expect(presenter.as_hash[:type]).to eq('credit') }
    it { expect(presenter.as_hash[:amount]).to eq(42) }
  end
end
