# frozen_string_literal: true

require './lib/rental_summary'

RSpec.describe RentalPresenter do
  subject(:presenter) { described_class.new(rental) }

  let(:car) { Car.new(id: 4, price_per_day: 1000, price_per_km: 10) }
  let(:rental) { Rental.new(rental_attributes) }
  let(:rental_attributes) do
    {
      id: 6,
      car: car,
      start_date: Date.new(2019, 04, 1),
      end_date: Date.new(2019, 04, 8),
      distance: 100,
      options: []
    }
  end
  let(:options) do
    [
      Option.new(id: 9, rental_id: 6, type: 'gps'),
      Option.new(id: 10, rental_id: 6, type: 'baby_seat')
    ]
  end

  it { expect(presenter.as_hash).to be_a(Hash) }
  it { expect(presenter.as_hash.keys).to eq(%i[id options actions]) }
  it { expect(presenter.as_hash[:options]).to eq([]) }
  it 'contains the name of the rental options' do
    rental_attributes[:options] = options
    expect(presenter.as_hash[:options]).to eq(%w[gps baby_seat])
  end
end
