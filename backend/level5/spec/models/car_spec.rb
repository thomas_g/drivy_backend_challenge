# frozen_string_literal: true

require './lib/rental_summary'

RSpec.describe Car do
  subject(:car) { described_class.new(car_attributes) }

  let(:car_attributes) { { id: 2, price_per_day: 1000, price_per_km: 5 } }

  describe 'Attributes' do
    it { expect(car.id).to eq(2) }
    it { expect(car.price_per_day).to eq(1000) }
    it { expect(car.price_per_km).to eq(5) }
  end

  describe 'Validations' do
    it 'raises an ArgumentError when :id is nil' do
      car_attributes[:id] = nil
      expect { car }.to raise_exception(ArgumentError, 'Id can\'t be blank.')
    end
    it 'raises an ArgumentError when :id is a float' do
      car_attributes[:id] = 3.14
      expect { car }.to raise_exception(ArgumentError, 'Id must be of type Integer.')
    end
    it 'raises an ArgumentError when :id is a string' do
      car_attributes[:id] = 'some string'
      expect { car }.to raise_exception(ArgumentError, 'Id must be of type Integer.')
    end
    it 'raises an ArgumentError when :id equals 0' do
      car_attributes[:id] = 0
      expect { car }.to raise_exception(ArgumentError, 'Id must be > 0.')
    end
    it 'raises an ArgumentError when :id negative' do
      car_attributes[:id] = -3
      expect { car }.to raise_exception(ArgumentError, 'Id must be > 0.')
    end
    it 'raises an ArgumentError when :price_per_day is nil' do
      car_attributes[:price_per_day] = nil
      expect { car }.to raise_exception(ArgumentError, 'Price_per_day can\'t be blank.')
    end
    it 'raises an ArgumentError when :price_per_day is a float' do
      car_attributes[:price_per_day] = 3.14
      expect { car }.to raise_exception(ArgumentError, 'Price_per_day must be of type Integer.')
    end
    it 'raises an ArgumentError when :price_per_day negative' do
      car_attributes[:price_per_day] = -12
      expect { car }.to raise_exception(ArgumentError, 'Price_per_day must be > 0.')
    end
    it 'raises an ArgumentError when :price_per_km is nil' do
      car_attributes[:price_per_km] = nil
      expect { car }.to raise_exception(ArgumentError, 'Price_per_km can\'t be blank.')
    end
    it 'raises an ArgumentError when :price_per_km is a string' do
      car_attributes[:price_per_km] = '100'
      expect { car }.to raise_exception(ArgumentError, 'Price_per_km must be of type Integer.')
    end
    it 'raises an ArgumentError when :price_per_km negative' do
      car_attributes[:price_per_km] = -423
      expect { car }.to raise_exception(ArgumentError, 'Price_per_km must be > 0.')
    end
  end
end
