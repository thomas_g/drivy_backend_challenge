# frozen_string_literal: true

require './lib/rental_summary'

RSpec.describe Rental do
  subject(:rental) { described_class.new(rental_attributes) }

  let(:car) { Car.new(id: 4, price_per_day: 1000, price_per_km: 10) }
  let(:options) do
    [Option.new(id: 4, rental_id: 6, type: 'baby_seat'),
     Option.new(id: 11, rental_id: 6, type: 'gps')]
  end
  let(:rental_attributes) do
    {
      id: 6,
      car: car,
      start_date: Date.new(2019, 04, 1),
      end_date: Date.new(2019, 04, 8),
      distance: 100,
      options: options
    }
  end

  describe 'Attributes' do
    it { expect(rental.id).to eq(6) }
    it { expect(rental.car).to eq(car) }
    it { expect(rental.start_date).to eq(Date.new(2019, 04, 1)) }
    it { expect(rental.end_date).to eq(Date.new(2019, 04, 8)) }
    it { expect(rental.distance).to eq(100) }
  end

  describe 'Validations' do
    it 'raises an ArgumentError when :id is nil' do
      rental_attributes[:id] = nil
      expect { rental }.to raise_exception(ArgumentError, 'Id can\'t be blank.')
    end
    it 'raises an ArgumentError when :id is a float' do
      rental_attributes[:id] = 6.14
      expect { rental }.to raise_exception(ArgumentError, 'Id must be of type Integer.')
    end
    it 'raises an ArgumentError when :id is equal 0' do
      rental_attributes[:id] = 0
      expect { rental }.to raise_exception(ArgumentError, 'Id must be > 0.')
    end
    it 'raises an ArgumentError when :car is nil' do
      rental_attributes[:car] = nil
      expect { rental }.to raise_exception(ArgumentError, 'Car can\'t be blank.')
    end
    it 'raises an ArgumentError when :car is a string' do
      rental_attributes[:car] = 'car'
      expect { rental }.to raise_exception(ArgumentError, 'Car must be of type Car.')
    end
    it 'raises an ArgumentError when :start_date is nil' do
      rental_attributes[:start_date] = nil
      expect { rental }.to raise_exception(ArgumentError, 'Start_date can\'t be blank.')
    end
    it 'raises an ArgumentError when :start_date is a string' do
      rental_attributes[:start_date] = '1998-12-24'
      expect { rental }.to raise_exception(ArgumentError, 'Start_date must be of type Date.')
    end
    it 'raises an ArgumentError when :end_date is nil' do
      rental_attributes[:end_date] = nil
      expect { rental }.to raise_exception(ArgumentError, 'End_date can\'t be blank.')
    end
    it 'raises an ArgumentError when :end_date is a string' do
      rental_attributes[:end_date] = '1998-12-24'
      expect { rental }.to raise_exception(ArgumentError, 'End_date must be of type Date.')
    end
    it 'raises an ArgumentError when :start_date smaller than :end_date is not a date' do
      rental_attributes[:start_date] = Date.new(2019, 04, 1)
      rental_attributes[:end_date] = Date.new(2019, 03, 1)
      expect { rental }.to raise_exception(ArgumentError, 'End_date must be >= start_date.')
    end
    it 'is valid when :start_date smaller equal :end_date' do
      rental_attributes[:start_date] = Date.new(2019, 04, 1)
      rental_attributes[:end_date] = Date.new(2019, 04, 1)
      expect(rental).to be_a described_class
    end
    it 'raises an ArgumentError when :distance is nil' do
      rental_attributes[:distance] = nil
      expect { rental }.to raise_exception(ArgumentError, 'Distance can\'t be blank.')
    end
    it 'raises an ArgumentError when :distance is a float' do
      rental_attributes[:distance] = 12.3
      expect { rental }.to raise_exception(ArgumentError, 'Distance must be of type Integer.')
    end
    it 'raises an ArgumentError when :distance is smaller than 0' do
      rental_attributes[:distance] = -8
      expect { rental }.to raise_exception(ArgumentError, 'Distance must be >= 0.')
    end
    it 'is valid when :distance equal 0' do
      rental_attributes[:distance] = 0
      expect(rental).to be_a described_class
    end
    it 'raises an ArgumentError when :options is not an array' do
      rental_attributes[:options] = 'some string'
      expect { rental }.to raise_exception(ArgumentError, 'Options must be of type Array.')
    end
  end

  describe 'Methods' do
    describe '#duration' do
      subject(:duration) { rental.duration }

      it 'returns the duration of 1 day' do
        rental_attributes[:start_date] = Date.new(2019, 01, 01)
        rental_attributes[:end_date] = Date.new(2019, 01, 01)

        expect(duration).to eq(1)
      end

      it 'returns the duration of 1 month' do
        rental_attributes[:start_date] = Date.new(2019, 01, 01)
        rental_attributes[:end_date] = Date.new(2019, 01, 31)

        expect(duration).to eq(31)
      end
    end

    describe '#fees_and_commissions' do
      subject(:commissions) { rental.fees_and_commissions }

      let(:rental) { described_class.new(rental_attributes) }
      let(:commissions_keys) { %i[driver owner insurance assistance drivy] }

      it { expect(commissions).to be_a(Hash) }
      it { expect(commissions.keys).to eq(commissions_keys) }
    end
  end
end
