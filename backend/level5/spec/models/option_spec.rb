# frozen_string_literal: true

require './lib/rental_summary'

RSpec.describe Option do
  subject(:option) { described_class.new(option_attributes) }

  let(:option_attributes) { { id: 2, rental_id: 42, type: 'gps' } }

  describe 'Attributes' do
    it { expect(option.id).to eq(2) }
    it { expect(option.rental_id).to eq(42) }
    it { expect(option.type).to eq('gps') }
  end

  describe 'Validations' do
    it 'raises an ArgumentError when :id is nil' do
      option_attributes[:id] = nil
      expect { option }.to raise_exception(ArgumentError, 'Id can\'t be blank.')
    end
    it 'raises an ArgumentError when :id is a float' do
      option_attributes[:id] = 3.14
      expect { option }.to raise_exception(ArgumentError, 'Id must be of type Integer.')
    end
    it 'raises an ArgumentError when :id is a string' do
      option_attributes[:id] = 'some string'
      expect { option }.to raise_exception(ArgumentError, 'Id must be of type Integer.')
    end
    it 'raises an ArgumentError when :id equals 0' do
      option_attributes[:id] = 0
      expect { option }.to raise_exception(ArgumentError, 'Id must be > 0.')
    end
    it 'raises an ArgumentError when :id negative' do
      option_attributes[:id] = -3
      expect { option }.to raise_exception(ArgumentError, 'Id must be > 0.')
    end
    it 'raises an ArgumentError when :rental_id is nil' do
      option_attributes[:rental_id] = nil
      expect { option }.to raise_exception(ArgumentError, 'Rental_id can\'t be blank.')
    end
    it 'raises an ArgumentError when :rental_id is a float' do
      option_attributes[:rental_id] = 3.14
      expect { option }.to raise_exception(ArgumentError, 'Rental_id must be of type Integer.')
    end
    it 'raises an ArgumentError when :rental_id negative' do
      option_attributes[:rental_id] = -12
      expect { option }.to raise_exception(ArgumentError, 'Rental_id must be > 0.')
    end
    it 'raises an ArgumentError when :type is nil' do
      option_attributes[:type] = nil
      expect { option }.to raise_exception(ArgumentError, 'Type can\'t be blank.')
    end
    it 'raises an ArgumentError when :type is a an integer' do
      option_attributes[:type] = 42
      expect { option }.to raise_exception(ArgumentError, 'Type must be of type String.')
    end
    it 'raises an ArgumentError when :type empty' do
      option_attributes[:type] = ''
      expect { option }.to raise_exception(ArgumentError,
                                           'Type can only be gps, baby_seat, additional_insurance.')
    end
    it 'raises an ArgumentError when :type is nitro' do
      option_attributes[:type] = 'nitro'
      expect { option }.to raise_exception(ArgumentError,
                                           'Type can only be gps, baby_seat, additional_insurance.')
    end
  end
end
