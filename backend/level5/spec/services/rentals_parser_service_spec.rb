# frozen_string_literal: true

require './lib/rental_summary'

RSpec.describe RentalsParserService do
  subject(:parser_result) { described_class.call(input_hash) }

  let(:input_hash) do
    {
      cars: [
        { id: 1, price_per_day: 2000, price_per_km: 10 },
        { id: 2, price_per_day: 1500, price_per_km: 15 },
        { id: 3, price_per_day: 1234, price_per_km: 15 }
      ],
      rentals: [
        { id: 1, car_id: 2, start_date: '2015-12-8', end_date: '2015-12-8', distance: 100 },
        { id: 2, car_id: 2, start_date: '2015-03-31', end_date: '2015-04-01', distance: 300 }
      ],
      options: [
        { id: 1, rental_id: 1, type: 'baby_seat' },
        { id: 2, rental_id: 1, type: 'gps' }
      ]
    }
  end

  context 'with valid hash' do
    it { expect(parser_result.map(&:class).uniq!).to eq([Rental]) }
    it { expect(parser_result.length).to be(2) }
    it { expect(parser_result.first.options.map(&:class).uniq!).to eq([Option]) }
    it { expect(parser_result.first.options.length).to eq(2) }
  end

  it 'is valid without options' do
    input_hash.delete(:options)
    expect(parser_result.length).to be(2)
  end

  it 'raises an ArgumentError with an empty hash' do
    input_hash.delete(:cars)
    input_hash.delete(:rentals)
    expect { parser_result }.to raise_exception(ArgumentError, 'Cars must be presents.')
  end

  it 'raises an ArgumentError without cars' do
    input_hash.delete(:cars)
    expect { parser_result }.to raise_exception(ArgumentError, 'Cars must be presents.')
  end

  it 'raises an ArgumentError with an empty cars array' do
    input_hash[:cars] = []
    expect { parser_result }.to raise_exception(ArgumentError, 'Cars must be presents.')
  end

  it 'raises an ArgumentError without rentals' do
    input_hash.delete(:rentals)
    expect { parser_result }.to raise_exception(ArgumentError, 'Rentals must be presents.')
  end

  it 'raises an ArgumentError with an empty rentals array' do
    input_hash[:rentals] = []
    expect { parser_result }.to raise_exception(ArgumentError, 'Rentals must be presents.')
  end

  it 'raises an ArgumentError with 2 cars having the same id' do
    input_hash[:cars].last[:id] = 1
    expect { parser_result }.to raise_exception(ArgumentError, 'Car ids must be unique.')
  end

  it 'raises an ArgumentError with 2 rentals having the same id' do
    input_hash[:rentals].last[:id] = 1
    expect { parser_result }.to raise_exception(ArgumentError, 'Rental ids must be unique.')
  end

  it 'raises an ArgumentError with 2 options having the same id' do
    input_hash[:options].last[:id] = 1
    expect { parser_result }.to raise_exception(ArgumentError, 'Option ids must be unique.')
  end

  it 'raises an ArgumentError with an overlapping rental period' do
    input_hash[:rentals].first[:start_date] = '2015-03-31'
    input_hash[:rentals].first[:end_date] = '2015-03-31'
    expected_exception = 'Rental periods can\'t be overlapping for the same car (car_id: 2).'
    expect { parser_result }.to raise_exception(ArgumentError, expected_exception)
  end
end
