# frozen_string_literal: true

require './lib/rental_summary'

RSpec.describe FeesAndCommissionsCalculatorService do
  subject(:calculator) { described_class.call(rental) }

  let(:car) { Car.new(id: 4, price_per_day: 1000, price_per_km: 10) }
  let(:option_gps)       { Option.new(id: 32, rental_id: 6, type: 'gps') }
  let(:option_baby)      { Option.new(id: 4, rental_id: 6, type: 'baby_seat') }
  let(:option_insurance) { Option.new(id: 11, rental_id: 6, type: 'additional_insurance') }
  let(:rental) { Rental.new(rental_attributes) }
  let(:rental_attributes) do
    {
      id: 6,
      car: car,
      distance: 100
    }
  end

  context 'with a rental period of 1 day' do
    before do
      rental_attributes[:start_date] = Date.new(2019, 04, 1)
      rental_attributes[:end_date] = Date.new(2019, 04, 1)
    end

    it 'calculates the commissions witout option' do
      rental_attributes[:options] = []
      expected_result = {
        driver: -2000,
        owner: 1400,
        insurance: 300,
        assistance: 100,
        drivy: 200
      }
      expect(calculator).to eq(expected_result)
    end
    it 'calculates the commissions with gps option' do
      rental_attributes[:options] = [option_gps]
      expected_result = {
        driver: -2500,
        owner: 1900,
        insurance: 300,
        assistance: 100,
        drivy: 200
      }
      expect(calculator).to eq(expected_result)
    end
    it 'calculates the commissions with baby seat option' do
      rental_attributes[:options] = [option_baby]
      expected_result = {
        driver: -2200,
        owner: 1600,
        insurance: 300,
        assistance: 100,
        drivy: 200
      }
      expect(calculator).to eq(expected_result)
    end
    it 'calculates the commissions with insurance option' do
      rental_attributes[:options] = [option_insurance]
      expected_result = {
        driver: -3000,
        owner: 1400,
        insurance: 300,
        assistance: 100,
        drivy: 1200
      }
      expect(calculator).to eq(expected_result)
    end
    it 'calculates the commissions with all 3 option' do
      rental_attributes[:options] = [option_gps, option_baby, option_insurance]
      expected_result = {
        driver: -3700,
        owner: 2100,
        insurance: 300,
        assistance: 100,
        drivy: 1200
      }
      expect(calculator).to eq(expected_result)
    end
  end

  context 'with a rental period of 3 days' do
    before do
      rental_attributes[:start_date] = Date.new(2019, 04, 1)
      rental_attributes[:end_date] = Date.new(2019, 04, 3)
    end

    it 'calculates the commissions witout option' do
      rental_attributes[:options] = []
      expected_result = {
        driver: -3800,
        owner: 2660,
        insurance: 570,
        assistance: 300,
        drivy: 270
      }
      expect(calculator).to eq(expected_result)
    end
    it 'calculates the commissions with all 3 option' do
      rental_attributes[:options] = [option_gps, option_baby, option_insurance]
      expected_result = {
        driver: -8900,
        owner: 4760,
        insurance: 570,
        assistance: 300,
        drivy: 3270
      }
      expect(calculator).to eq(expected_result)
    end
  end

  it 'calculates the commissions witout option for a period of 6 days' do
    rental_attributes[:start_date] = Date.new(2019, 04, 1)
    rental_attributes[:end_date] = Date.new(2019, 04, 6)
    rental_attributes[:options] = []
    expected_result = {
      driver: -6100,
      owner: 4270,
      insurance: 915,
      assistance: 600,
      drivy: 315
    }
    expect(calculator).to eq(expected_result)
  end

  it 'calculates the commissions witout option for a period of 15 days' do
    rental_attributes[:start_date] = Date.new(2019, 04, 1)
    rental_attributes[:end_date] = Date.new(2019, 04, 15)
    rental_attributes[:options] = []
    expected_result = {
      driver: -11_400,
      owner: 7980,
      insurance: 1710,
      assistance: 1500,
      drivy: 210
    }
    expect(calculator).to eq(expected_result)
  end

  it 'calculates a negative commission for Drivy when the price is too small' do
    rental_attributes[:start_date] = Date.new(2019, 04, 1)
    rental_attributes[:end_date] = Date.new(2019, 04, 3)
    rental_attributes[:options] = []
    rental_attributes[:car] = Car.new(id: 4, price_per_day: 10, price_per_km: 10)

    expect(calculator[:drivy]).to eq(-145)
  end
end
