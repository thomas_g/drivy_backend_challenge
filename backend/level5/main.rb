# frozen_string_literal: true

require_relative 'lib/rental_summary'

RentalsSummaryGeneratorService.call('data/input.json', 'data/output.json')
