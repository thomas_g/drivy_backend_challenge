# frozen_string_literal: true

class RentalsPresenter
  def self.as_hash(rentals)
    new(rentals).as_hash
  end

  def as_hash
    {
      rentals: @rentals.map { |rental| RentalPresenter.as_hash(rental) }
    }
  end

  private

  def initialize(rentals)
    @rentals = rentals
  end
end
