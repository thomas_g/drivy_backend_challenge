# frozen_string_literal: true

class RentalPresenter
  def self.as_hash(rental)
    new(rental).as_hash
  end

  def as_hash
    {
      id: @rental.id,
      options: @rental.options.map(&:type),
      actions: @rental.fees_and_commissions.map do |actor, value|
        ActionPresenter.as_hash(actor, value)
      end
    }
  end

  private

  def initialize(rental)
    @rental = rental
  end
end
