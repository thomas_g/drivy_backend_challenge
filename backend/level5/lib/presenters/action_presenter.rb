# frozen_string_literal: true

class ActionPresenter
  def self.as_hash(actor, value)
    new(actor, value).as_hash
  end

  def as_hash
    {
      who: @actor.to_s,
      type: @value.negative? ? 'debit' : 'credit',
      amount: @value.abs
    }
  end

  private

  def initialize(actor, value)
    @actor = actor
    @value = value
  end
end
