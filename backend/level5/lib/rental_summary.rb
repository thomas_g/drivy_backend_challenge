# frozen_string_literal: true

require 'json'
require 'date'
require_relative 'constants'
require_relative 'models/model'
require_relative 'models/car'
require_relative 'models/option'
require_relative 'models/rental'
require_relative 'services/rentals_summary_generator_service'
require_relative 'services/rentals_parser_service'
require_relative 'services/fees_and_commissions_calculator_service'
require_relative 'presenters/rentals_presenter'
require_relative 'presenters/rental_presenter'
require_relative 'presenters/action_presenter'

module RentalSummary; end
