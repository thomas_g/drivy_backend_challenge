# frozen_string_literal: true

class RentalsSummaryGeneratorService
  def self.call(input_path, output_path)
    new(input_path, output_path).call
  end

  def call
    rentals = get_rentals_from_hash(input_hash)
    write_to_json_file RentalsPresenter.as_hash(rentals)
  end

  private

  def initialize(input_path, output_path)
    @input_path = input_path
    @output_path = output_path
  end

  def input_hash
    input_file = File.read(@input_path)
    JSON.parse(input_file, symbolize_names: true)
  end

  def get_rentals_from_hash(input_hash)
    RentalsParserService.call(input_hash)
  end

  def write_to_json_file(hash)
    File.open(@output_path, 'w') do |f|
      # Always end a text file with a eol char (\n)
      # OPTIMIZE find a better way to properly format empty arrays
      f.write(JSON.pretty_generate(hash).gsub(/\[\n\n\s+\]/, '[]') + "\n")
      p "The summary of the rentals have been written in the file #{@output_path}."
    end
  end
end
