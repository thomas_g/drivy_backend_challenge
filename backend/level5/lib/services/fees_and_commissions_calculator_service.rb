# frozen_string_literal: true

class FeesAndCommissionsCalculatorService
  def self.call(rental)
    new(rental).call
  end

  def call
    {
      driver: driver_ballance.to_i,
      owner: owner_ballance.to_i,
      insurance: insurance_ballance.to_i,
      assistance: assistance_ballance.to_i,
      drivy: drivy_ballance.to_i
    }
  end

  private

  def initialize(rental)
    @rental = rental
  end

  def gross_price
    distance_rate + duration_rate
  end

  def distance_rate
    @rental.distance * @rental.car.price_per_km
  end

  def duration_rate
    # apply discount after a certain amount of days
    duration = @rental.duration
    DURATION_DISOUNTS.map do |bracket|
      discount = duration > bracket[:days] ? 1 - percent_to_f(bracket[:discount]) : 0
      rate = (duration - bracket[:days]) * @rental.car.price_per_day * discount
      duration = bracket[:days] if discount.positive?
      rate
    end.sum + duration * @rental.car.price_per_day
  end

  def charges
    gross_price * percent_to_f(TOTAL_CHARGES_PERCENT)
  end

  def driver_ballance
    ballance = gross_price
    ballance += option_price_for('gps')
    ballance += option_price_for('baby_seat')
    ballance += option_price_for('additional_insurance')
    ballance * -1
  end

  def owner_ballance
    ballance = gross_price - charges
    ballance += option_price_for('gps')
    ballance += option_price_for('baby_seat')
    ballance
  end

  def insurance_ballance
    charges * percent_to_f(INSURANCE_PERCENT_ON_CHARGES)
  end

  def assistance_ballance
    @rental.duration * DAILY_ASSISTANCE_RATE
  end

  def drivy_ballance
    ballance = charges - insurance_ballance - assistance_ballance
    ballance += option_price_for('additional_insurance')
    ballance
  end

  def option_price_for(option_name)
    option = @rental.options.find { |opt| opt.type == option_name }
    option ? @rental.duration * OPTIONS[option_name.to_sym] : 0
  end

  def percent_to_f(percent)
    percent.to_f / 100
  end
end
