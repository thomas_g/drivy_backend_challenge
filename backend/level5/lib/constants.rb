# frozen_string_literal: true

# Price of each option
OPTIONS = {
  gps: 500,
  baby_seat: 200,
  additional_insurance: 1000
}.freeze

# Discount percentage after a given number of rental days
# Array order matters !!
DURATION_DISOUNTS = [
  { days: 10, discount: 50 },
  { days: 4, discount: 30 },
  { days: 1, discount: 10 }
].freeze

# Commissions
TOTAL_CHARGES_PERCENT = 30
# Percentage of charges taken by the insurance
INSURANCE_PERCENT_ON_CHARGES = 50
DAILY_ASSISTANCE_RATE = 100
