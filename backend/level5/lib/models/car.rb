# frozen_string_literal: true

class Car < Model
  attr_reader :id, :price_per_day, :price_per_km

  ATTRIBUTES = {
    id: {
      presence: true,
      type: Integer,
      condition: '> 0'
    },
    price_per_day: {
      presence: true,
      type: Integer,
      condition: '> 0'
    },
    price_per_km: {
      presence: true,
      type: Integer,
      condition: '> 0'
    }
  }.freeze
end
