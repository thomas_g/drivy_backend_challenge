# frozen_string_literal: true

class Rental < Model
  attr_reader :id, :car, :start_date, :end_date, :distance, :options

  ATTRIBUTES = {
    id: {
      presence: true,
      type: Integer,
      condition: '> 0'
    },
    car: {
      presence: true,
      type: Car
    },
    start_date: {
      presence: true,
      type: Date
    },
    end_date: {
      presence: true,
      type: Date,
      condition: '>= start_date'
    },
    distance: {
      presence: true,
      type: Integer,
      condition: '>= 0'
    },
    options: {
      type: Array
    }
  }.freeze

  def duration
    (end_date - start_date).to_i + 1
  end

  def fees_and_commissions
    FeesAndCommissionsCalculatorService.call(self)
  end
end
