# frozen_string_literal: true

class Option < Model
  attr_reader :id, :rental_id, :type

  ATTRIBUTES = {
    id: {
      presence: true,
      type: Integer,
      condition: '> 0'
    },
    rental_id: {
      presence: true,
      type: Integer,
      condition: '> 0'
    },
    type: {
      presence: true,
      type: String,
      inclusion: %w[gps baby_seat additional_insurance]
    }
  }.freeze
end
