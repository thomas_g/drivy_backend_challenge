# frozen_string_literal: true

class Model
  private

  def initialize(params = {})
    self.class::ATTRIBUTES.each { |k, _v| instance_variable_set("@#{k}", params[k]) }
    validate
  end

  def validate
    self.class::ATTRIBUTES.each do |attr_name, validation|
      validate_presence(attr_name) if validation[:presence]
      next unless validation[:presence] || attribute_from_name(attr_name)

      validate_type(attr_name, validation[:type]) if validation[:type]
      validate_condition(attr_name, validation[:condition]) if validation[:condition]
      validate_inclusion(attr_name, validation[:inclusion]) if validation[:inclusion]
    end
  end

  def validate_presence(attr_name)
    raise ArgumentError, "#{attr_name.capitalize} can't be blank." if
      attribute_from_name(attr_name).nil? # || attribute_from_name(attr_name).empty?
  end

  def validate_type(attr_name, type)
    raise ArgumentError, "#{attr_name.capitalize} must be of type #{type}." unless
      attribute_from_name(attr_name).is_a?(type)
  end

  def validate_condition(attr_name, condition)
    raise ArgumentError, "#{attr_name.capitalize} must be #{condition}." unless
      eval("#{attr_name} #{condition}")
  end

  def validate_inclusion(attr_name, values)
    raise ArgumentError, "#{attr_name.capitalize} can only be #{values.join(', ')}." unless
      values.include?(attribute_from_name(attr_name))
  end

  def attribute_from_name(attr_name)
    send(attr_name)
  end
end
