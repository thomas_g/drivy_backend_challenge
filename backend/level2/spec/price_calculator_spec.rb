require './main.rb'

RSpec.describe PriceCalculator do
  subject(:price_calculator) { described_class.new }

  describe '#call' do
    after { File.delete('./data/output.json') }

    it 'calculates the price of the rentals and save it as a json file' do
      price_calculator.call
      expect(FileUtils).to be_identical('./data/output.json', './data/expected_output.json')
    end
  end
end
