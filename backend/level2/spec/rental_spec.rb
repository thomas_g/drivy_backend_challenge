require './main.rb'

RSpec.describe Rental do
  let(:car) { Car.new(id: 4, price_per_day: 1000, price_per_km: 10) }
  let(:rental_attributes) do
    {
      id: 6,
      car: car,
      start_date: Date.new(2019,04,1),
      end_date: Date.new(2019,04,8),
      distance: 100
    }
  end
  subject(:rental) { described_class.new(rental_attributes) }

  describe '#initialize' do
    it { expect(rental.id).to eq(6) }
    it { expect(rental.car).to eq(car) }
    it { expect(rental.start_date).to eq(Date.new(2019,04,1)) }
    it { expect(rental.end_date).to eq(Date.new(2019,04,8)) }
    it { expect(rental.distance).to eq(100) }
  end

  describe '#price' do
    it 'calculates the price for a rental period of 1 day' do
      rental_attributes[:end_date] = Date.new(2019,04,1)
      expect(rental.price).to eq(2000)
    end
    it 'calculates the price for a rental period of 2 day' do
      rental_attributes[:end_date] = Date.new(2019,04,2)
      expect(rental.price).to eq(2900)
    end
    it 'calculates the price for a rental period of 5 day' do
      rental_attributes[:end_date] = Date.new(2019,04,5)
      expect(rental.price).to eq(5400)
    end
    it 'calculates the price for a rental period of 20 day' do
      rental_attributes[:end_date] = Date.new(2019,04,20)
      expect(rental.price).to eq(13900)
    end
  end
end
