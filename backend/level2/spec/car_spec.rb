require './main.rb'

RSpec.describe Car do
  subject(:car) { described_class.new(id: 2, price_per_day: 1000, price_per_km: 5) }

  describe '#initialize' do
    it { expect(car.id).to eq(2) }
    it { expect(car.price_per_day).to eq(1000) }
    it { expect(car.price_per_km).to eq(5) }
  end
end
