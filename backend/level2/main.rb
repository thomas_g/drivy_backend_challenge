require 'json'
require 'date'

class Car
  attr_reader :id, :price_per_day, :price_per_km

  def initialize(params = {})
    @id = params[:id]
    @price_per_day = params[:price_per_day]
    @price_per_km = params[:price_per_km]
  end
end

class Rental
  attr_reader :id, :car, :start_date, :end_date, :distance

  def initialize(params = {})
    @id = params[:id]
    @car = params[:car]
    @start_date = params[:start_date]
    @end_date = params[:end_date]
    @distance = params[:distance]
  end

  def price
    rental_length = (end_date - start_date).to_i + 1
    distance_rate = distance * car.price_per_km

    # apply discount after a certain amount of days
    duration_rate = 0
    rental_length.times do |day|
      case day
      when 0
        duration_rate += car.price_per_day
      when 1..3
        duration_rate += car.price_per_day * 0.9
      when 4..9
        duration_rate += car.price_per_day * 0.7
      else
        duration_rate += car.price_per_day * 0.5
      end
    end

    distance_rate + duration_rate.to_i
  end
end

class PriceCalculator
  def call
    input_file = File.read('./data/input.json')
    input = JSON.parse(input_file)

    cars = input['cars'].map do |car|
      Car.new(
        id: car['id'],
        price_per_day: car['price_per_day'],
        price_per_km: car['price_per_km']
      )
    end

    rentals = input['rentals'].map do |rental|
      Rental.new(
        id: rental['id'],
        car: cars.detect{ |car| car.id == rental['car_id'] },
        start_date: Date.strptime(rental['start_date'], '%Y-%m-%d'),
        end_date: Date.strptime(rental['end_date'], '%Y-%m-%d'),
        distance: rental['distance']
      )
    end

    output = { rentals: [] }
    rentals.each do |rental|
      output[:rentals] << {
        id: rental.id,
        price: rental.price
      }
    end

    File.open('./data/output.json','w') do |f|
      # End the file with a eol char (\n)
      f.write(JSON.pretty_generate(output) + "\n")
    end
  end
end

# Do not execute if called by another file (ie: Rspec)
if $PROGRAM_NAME == __FILE__
  PriceCalculator.new.call
end
