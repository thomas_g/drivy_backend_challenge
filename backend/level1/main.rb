require 'json'
require 'date'

class PriceCalculator
  def call
    input_file = File.read('./data/input.json')
    input = JSON.parse(input_file)

    output = { rentals: [] }
    input['rentals'].each do |rental|
      car = input['cars'].detect { |car| car['id'] == rental['car_id'] }
      start_date = Date.strptime(rental['start_date'], '%Y-%m-%d')
      end_date = Date.strptime(rental['end_date'], '%Y-%m-%d')
      days = (end_date - start_date).to_i + 1
      time_price = days * car['price_per_day']
      distance_price = rental['distance'] * car['price_per_km']

      output[:rentals] << {
        id: rental['id'],
        price: time_price + distance_price
      }
    end

    File.open('./data/output.json','w') do |f|
      # End the file with a eol char (\n)
      f.write(JSON.pretty_generate(output) + "\n")
    end
  end
end

# Do not execute if called by another file (ie: Rspec)
if $PROGRAM_NAME == __FILE__
  PriceCalculator.new.call
end
