require './lib/rental_summary_generator'

RSpec.describe RentalSummaryGenerator do
  describe '#get_rentals_from_hash' do
    let(:valid_hash) do
      {
        cars: [
          { id: 1, price_per_day: 2000, price_per_km: 10 },
          { id: 2, price_per_day: 2000, price_per_km: 10 }
        ],
        rentals: [
          { id: 1, car_id: 1, start_date: '2015-12-8', end_date: '2015-12-8', distance: 100 },
          { id: 2, car_id: 1, start_date: '2015-03-31', end_date: '2015-04-01', distance: 300 }
        ]
      }
    end
    subject(:rentals) { described_class.new.get_rentals_from_hash(input_hash) }

    context 'with an empty hash' do
      let(:input_hash) {{}}
      it { expect(rentals).to be_an(Array) }
      it { expect(rentals).to be_empty }
    end

    context 'without cars' do
      let(:input_hash) { valid_hash.tap { |h| h.delete(:cars) } }
      it { expect(rentals).to eq([]) }
    end

    context 'with an empty cars array' do
      let(:input_hash) { valid_hash.tap { |h| h[:cars] = [] } }
      it { expect(rentals).to eq([]) }
    end

    context 'without rentals' do
      let(:input_hash) { valid_hash.tap { |h| h.delete(:rentals) } }
      it { expect(rentals).to eq([]) }
    end

    context 'with an empty rentals array' do
      let(:input_hash) { valid_hash.tap { |h| h[:rentals] = [] } }
      it { expect(rentals).to eq([]) }
    end

    context 'with 2 cars having the same id' do
      let(:input_hash) { valid_hash.tap { |h| h[:cars].last[:id] = 1 } }

      it 'raises an ArgumentError' do
        expect { rentals }.to raise_exception(ArgumentError, 'car ids must be unique')
      end
    end

    context 'with 2 rentals having the same id' do
      let(:input_hash) { valid_hash.tap { |h| h[:rentals].last[:id] = 1 } }
      it 'raises an ArgumentError' do
        expect { rentals }.to raise_exception(ArgumentError, 'rental ids must be unique')
      end
    end

    context 'with 2 rentals having the same car overlapping' do
      let(:input_hash) do
        valid_hash.tap do |h|
          h[:rentals].first[:start_date] = '2015-03-31'
          h[:rentals].first[:end_date] = '2015-03-31'
        end
      end
      it 'raises an ArgumentError' do
        expect { rentals }.to raise_exception(ArgumentError,
          'rental period can\'t be overlapping for the same car')
      end
    end

    context 'with valid hash' do
      let(:input_hash) { valid_hash }
      it { expect(rentals.map(&:class).uniq!).to eq([Rental]) }
      it { expect(rentals.length).to be(2) }
    end
  end
end
