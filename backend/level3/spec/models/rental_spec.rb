require './lib/rental_summary_generator'

RSpec.describe Rental do
  let(:car) { Car.new(id: 4, price_per_day: 1000, price_per_km: 10) }
  let(:rental_attributes) do
    {
      id: 6,
      car: car,
      start_date: Date.new(2019,04,1),
      end_date: Date.new(2019,04,8),
      distance: 100
    }
  end
  subject(:rental) { described_class.new(rental_attributes) }

  describe '#initialize' do
    it { expect(rental.id).to eq(6) }
    it { expect(rental.car).to eq(car) }
    it { expect(rental.start_date).to eq(Date.new(2019,04,1)) }
    it { expect(rental.end_date).to eq(Date.new(2019,04,8)) }
    it { expect(rental.distance).to eq(100) }

    it 'raises an ArgumentError when :id is nil' do
      rental_attributes[:id] = nil
      expect { rental }.to raise_exception(ArgumentError, 'id must be an integer')
    end
    it 'raises an ArgumentError when :id is a float' do
      rental_attributes[:id] = 6.14
      expect { rental }.to raise_exception(ArgumentError, 'id must be an integer')
    end
    it 'raises an ArgumentError when :id is equal 0' do
      rental_attributes[:id] = 0
      expect { rental }.to raise_exception(ArgumentError, 'id must be greater than 0')
    end
    it 'raises an ArgumentError when :car is not a Car' do
      rental_attributes[:car] = 'car'
      expect { rental }.to raise_exception(ArgumentError, 'car must be a car')
    end
    it 'raises an ArgumentError when :start_date is not a date' do
      rental_attributes[:start_date] = '1998-12-24'
      expect { rental }.to raise_exception(ArgumentError, 'start_date must be a date')
    end
    it 'raises an ArgumentError when :end_date is not a date' do
      rental_attributes[:end_date] = '1998-12-24'
      expect { rental }.to raise_exception(ArgumentError, 'end_date must be a date')
    end
    it 'raises an ArgumentError when :start_date smaller than :end_date is not a date' do
      rental_attributes[:start_date] = Date.new(2019,04,1)
      rental_attributes[:end_date] = Date.new(2019,03,1)
      expect { rental }.to raise_exception(ArgumentError, 'end_date can\'t be smaller than start_date')
    end
    it 'is valid when :start_date smaller equal :end_date' do
      rental_attributes[:start_date] = Date.new(2019,04,1)
      rental_attributes[:end_date] = Date.new(2019,04,1)
      expect(rental).to be_a Rental
    end
    it 'raises an ArgumentError when :distance is not an int' do
      rental_attributes[:distance] = 12.3
      expect { rental }.to raise_exception(ArgumentError, 'distance must be a an integer')
    end
    it 'raises an ArgumentError when :distance is smaller than 0' do
      rental_attributes[:distance] = -8
      expect { rental }.to raise_exception(ArgumentError, 'distance must be greater than or equal to zero')
    end
    it 'is valid when :distance equal 0' do
      rental_attributes[:distance] = 0
      expect(rental).to be_a Rental
    end
  end

  describe '#price' do
    it 'calculates the price for a rental period of 1 day' do
      rental_attributes[:end_date] = Date.new(2019,04,1)
      expect(rental.price).to eq(2000)
    end
    it 'calculates the price for a rental period of 2 day' do
      rental_attributes[:end_date] = Date.new(2019,04,2)
      expect(rental.price).to eq(2900)
    end
    it 'calculates the price for a rental period of 5 day' do
      rental_attributes[:end_date] = Date.new(2019,04,5)
      expect(rental.price).to eq(5400)
    end
    it 'calculates the price for a rental period of 20 day' do
      rental_attributes[:end_date] = Date.new(2019,04,20)
      expect(rental.price).to eq(13900)
    end
  end

  describe '#commission' do
    it 'calculates the commission for a price of 12000 and 8 days' do
      allow(rental).to receive(:price).and_return(12000)
      expected_result = { insurance_fee: 1800, assistance_fee: 800, drivy_fee: 1000 }
      expect(rental.commission).to eq(expected_result)
    end
    it 'calculates the commission for a price of 12000 and 1 days' do
      rental_attributes[:end_date] = Date.new(2019,04,1)
      allow(rental).to receive(:price).and_return(12000)
      expected_result = { insurance_fee: 1800, assistance_fee: 100, drivy_fee: 1700 }
      expect(rental.commission).to eq(expected_result)
    end
    it 'calculates the commission for a price of 1000 and 8 days' do
      allow(rental).to receive(:price).and_return(1000)
      expected_result = { insurance_fee: 150, assistance_fee: 800, drivy_fee: -650 }
      expect(rental.commission).to eq(expected_result)
    end
  end
end
