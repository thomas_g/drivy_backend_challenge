require './lib/rental_summary_generator'

RSpec.describe Car do
  let(:car_attributes) { {id: 2, price_per_day: 1000, price_per_km: 5} }
  subject(:car) { described_class.new(car_attributes) }

  describe '#initialize' do
    it { expect(car.id).to eq(2) }
    it { expect(car.price_per_day).to eq(1000) }
    it { expect(car.price_per_km).to eq(5) }
    it 'raises an ArgumentError when :id is nil' do
      car_attributes[:id] = nil
      expect { car }.to raise_exception(ArgumentError, 'id must be an integer')
    end
    it 'raises an ArgumentError when :id is a float' do
      car_attributes[:id] = 3.14
      expect { car }.to raise_exception(ArgumentError, 'id must be an integer')
    end
    it 'raises an ArgumentError when :id is a string' do
      car_attributes[:id] = 'some string'
      expect { car }.to raise_exception(ArgumentError, 'id must be an integer')
    end
    it 'raises an ArgumentError when :id equals 0' do
      car_attributes[:id] = 0
      expect { car }.to raise_exception(ArgumentError, 'id must be greater than 0')
    end
    it 'raises an ArgumentError when :id negative' do
      car_attributes[:id] = -3
      expect { car }.to raise_exception(ArgumentError, 'id must be greater than 0')
    end
    it 'raises an ArgumentError when :price_per_day is nil' do
      car_attributes[:price_per_day] = nil
      expect { car }.to raise_exception(ArgumentError, 'price_per_day must be an integer')
    end
    it 'raises an ArgumentError when :price_per_day negative' do
      car_attributes[:price_per_day] = -12
      expect { car }.to raise_exception(ArgumentError, 'price_per_day must be greater than 0')
    end
    it 'raises an ArgumentError when :price_per_km is a string' do
      car_attributes[:price_per_km] = '100'
      expect { car }.to raise_exception(ArgumentError, 'price_per_km must be an integer')
    end
    it 'raises an ArgumentError when :price_per_km negative' do
      car_attributes[:price_per_km] = -423
      expect { car }.to raise_exception(ArgumentError, 'price_per_km must be greater than 0')
    end
  end
end
