require 'json'
require 'date'
require_relative 'models/car'
require_relative 'models/rental'

class RentalSummaryGenerator
  def call
    input_file = File.read('./data/input.json')
    input_hash = JSON.parse(input_file, { symbolize_names: true })

    rentals = get_rentals_from_hash(input_hash)

    output = { rentals: [] }
    rentals.each do |rental|
      output[:rentals] << {
        id: rental.id,
        price: rental.price,
        commission: rental.commission
      }
    end

    File.open('./data/output.json','w') do |f|
      # Always end a text file with a eol char (\n)
      f.write(JSON.pretty_generate(output) + "\n")
    end
  end

  def get_rentals_from_hash(input_hash)
    return [] if input_hash[:cars].nil? || input_hash[:cars].empty?
    return [] if input_hash[:rentals].nil? || input_hash[:rentals].empty?

    if input_hash[:cars].map{|car| car[:id]}.uniq.count != input_hash[:cars].length
      raise ArgumentError, 'car ids must be unique'
    end
    cars = input_hash[:cars].map do |car|
      Car.new(
        id: car[:id],
        price_per_day: car[:price_per_day],
        price_per_km: car[:price_per_km]
      )
    end

    if input_hash[:rentals].map{|rental| rental[:id]}.uniq.count != input_hash[:rentals].length
      raise ArgumentError, 'rental ids must be unique'
    end
    rentals = []
    input_hash[:rentals].each do |rental|
      car = cars.find{ |car| car.id == rental[:car_id] }
      new_rental = Rental.new(
        id: rental[:id],
        car: car,
        start_date: Date.strptime(rental[:start_date], '%Y-%m-%d'),
        end_date: Date.strptime(rental[:end_date], '%Y-%m-%d'),
        distance: rental[:distance]
      )
      # Check if there is already a rental with the same car that has an overlapping rental period
      rentals.find_all {|r| r.car == car}.each do |candidate|
        if new_rental.start_date <= candidate.end_date && 
          candidate.start_date <= new_rental.end_date
          raise ArgumentError, 'rental period can\'t be overlapping for the same car'
        end
      end

      rentals << new_rental
    end
    rentals
  end
end
