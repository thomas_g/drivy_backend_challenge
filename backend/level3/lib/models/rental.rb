class Rental
  attr_reader :id, :car, :start_date, :end_date, :distance

  def initialize(params = {})
    @id = params[:id]
    raise ArgumentError, 'id must be an integer' unless @id.is_a?(Integer)
    raise ArgumentError, 'id must be greater than 0' unless @id > 0

    @car = params[:car]
    raise ArgumentError, 'car must be a car' unless @car.is_a?(Car)

    @start_date = params[:start_date]
    raise ArgumentError, 'start_date must be a date' unless @start_date.is_a?(Date)

    @end_date = params[:end_date]
    raise ArgumentError, 'end_date must be a date' unless @end_date.is_a?(Date)

    if @end_date < @start_date
      raise ArgumentError, 'end_date can\'t be smaller than start_date'
    end

    @distance = params[:distance]
    raise ArgumentError, 'distance must be a an integer' unless @distance.is_a?(Integer)
    raise ArgumentError, 'distance must be greater than or equal to zero' if @distance < 0
  end

  def price
    distance_rate + duration_rate
  end

  def commission
    duration = (end_date - start_date).to_i + 1
    {
      insurance_fee: (price * 0.15).to_i,
      assistance_fee: duration * 100,
      drivy_fee: (price * 0.15 - duration * 100).to_i
    }
  end

  private

  def distance_rate
    distance * car.price_per_km
  end

  def duration_rate
    duration = (end_date - start_date).to_i + 1
    # apply discount after a certain amount of days
    duration_rate = 0
    duration.times do |day|
      case day
      when 0
        duration_rate += car.price_per_day
      when 1..3
        duration_rate += car.price_per_day * 0.9
      when 4..9
        duration_rate += car.price_per_day * 0.7
      else
        duration_rate += car.price_per_day * 0.5
      end
    end
    duration_rate.to_i
  end

end
