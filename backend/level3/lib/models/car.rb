class Car
  attr_reader :id, :price_per_day, :price_per_km

  def initialize(params = {})
    @id = params[:id]
    raise ArgumentError, 'id must be an integer' unless @id.is_a?(Integer)
    raise ArgumentError, 'id must be greater than 0' unless @id > 0

    @price_per_day = params[:price_per_day]
    raise ArgumentError, 'price_per_day must be an integer' unless @price_per_day.is_a?(Integer)
    raise ArgumentError, 'price_per_day must be greater than 0' unless @price_per_day > 0

    @price_per_km = params[:price_per_km]
    raise ArgumentError, 'price_per_km must be an integer' unless @price_per_km.is_a?(Integer)
    raise ArgumentError, 'price_per_km must be greater than 0' unless @price_per_km > 0
  end
end
